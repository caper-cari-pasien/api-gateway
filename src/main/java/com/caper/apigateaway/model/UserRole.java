package com.caper.apigateaway.model;

import com.google.common.collect.Sets;
import java.util.Set;
import static com.caper.apigateaway.model.UserPermission.*;

public enum UserRole {

    DOCTOR(Sets.newHashSet(DOCTOR_READ, DOCTOR_UPDATE)),
    PATIENT(Sets.newHashSet(PATIENT_READ, PATIENT_UPDATE));

    private final Set<UserPermission> permissions;

    UserRole(Set<UserPermission> permissions) {
        this.permissions = permissions;
    }

    public Set<UserPermission> getPermissions() {
        return permissions;
    }


}
