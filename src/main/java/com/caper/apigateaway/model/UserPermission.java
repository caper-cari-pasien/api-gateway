package com.caper.apigateaway.model;

public enum UserPermission {
    DOCTOR_READ("dokter:read"),
    DOCTOR_UPDATE("dokter:update"),
    PATIENT_READ("pasien:read"),
    PATIENT_UPDATE("pasien:update");
    private final String permission;

    UserPermission(String permission) {
        this.permission = permission;
    }

    public String getPermission() {
        return permission;
    }
}
