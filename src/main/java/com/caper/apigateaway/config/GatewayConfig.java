package com.caper.apigateaway.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class GatewayConfig {

    @Value("${service-uri.auth}")
    String authUri;

    @Value("${service-uri.profile}")
    String profileUri;

    @Value("${service-uri.treatment}")
    String treatmentUri;

    @Value("${service-uri.chat}")
    String chatUri;
    private static final String API_SEGMENT = "/api${segment}";
    @Bean
    public RouteLocator routes(RouteLocatorBuilder builder, JwtAuthenticationFilter filter) {
        return builder.routes()
            .route("auth-service", r -> r.path("/api/auth/**")
                    .filters(f -> f.filter(filter).rewritePath("/api/auth(?<segment>/?.*)", API_SEGMENT))
                    .uri(authUri))
            .route("profile-service", r -> r.path("/api/profile/**")
                    .filters(f -> f.filter(filter).rewritePath("/api/profile(?<segment>/?.*)", API_SEGMENT))
                    .uri(profileUri))
            .route("treatment-service", r -> r.path("/api/treatment/**")
                    .filters(f -> f.filter(filter).rewritePath("/api/treatment(?<segment>/?.*)", API_SEGMENT))
                    .uri(treatmentUri))
            .route("chat-service", r -> r.path("/api/chat/**")
                    .filters(f -> f.filter(filter).rewritePath("/api/chat(?<segment>/?.*)", API_SEGMENT))
                    .uri(chatUri))
            .build();

    }
}
