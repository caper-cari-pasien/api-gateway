package com.caper.apigateaway.config;

import com.caper.apigateaway.service.JwtService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.cloud.gateway.filter.GatewayFilter;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.core.io.buffer.DefaultDataBufferFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RefreshScope
@Component
public class JwtAuthenticationFilter implements GatewayFilter {

    @Autowired
    JwtService jwtService;

    @Autowired
    private RouterValidator routerValidator;
    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
        ServerHttpRequest request = exchange.getRequest();

        if (routerValidator.getIsSecured().test(request)) {
            if (isAuthMissing(request))
                return onError(exchange, "Authorization header is missing in request", HttpStatus.UNAUTHORIZED);

            try{
                final var token = getAuthHeader(request).substring(7);

                if (jwtService.isTokenExpired(token)){
                    return onError(exchange, "Authorization header is invalid", HttpStatus.UNAUTHORIZED);
                }

                populateRequestWithHeaders(exchange, token);
            }catch (IndexOutOfBoundsException e){
                return onError(exchange, "Authorization header is invalid", HttpStatus.UNAUTHORIZED);
            }

        }
        return chain.filter(exchange);
    }

    private Mono<Void> onError(ServerWebExchange exchange, String err, HttpStatus httpStatus) {
        ServerHttpResponse response = exchange.getResponse();
        response.setStatusCode(httpStatus);
        return response.writeWith(Flux.just(new DefaultDataBufferFactory().wrap(err.getBytes())));
    }

    private boolean isAuthMissing(ServerHttpRequest request){
        return !request.getHeaders().containsKey("Authorization");
    }

    private String getAuthHeader(ServerHttpRequest request) {
        return request.getHeaders().getOrEmpty("Authorization").get(0);
    }

    private void populateRequestWithHeaders(ServerWebExchange exchange, String token) {
        var claims = jwtService.extractAllClaims(token);
        exchange.getRequest().mutate()
                .header("username", String.valueOf(claims.get("username")))
                .header("role", String.valueOf(claims.get("role")))
                .build();
    }
}

