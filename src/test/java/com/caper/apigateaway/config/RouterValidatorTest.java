package com.caper.apigateaway.config;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.http.server.reactive.ServerHttpRequest;

import java.net.URI;
import java.net.URISyntaxException;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class RouterValidatorTest {

    @Test
     void testIsSecured() {
        // Create a mock ServerHttpRequest with a secured path
        ServerHttpRequest securedRequest = mock(ServerHttpRequest.class);
        try {
            when(securedRequest.getURI()).thenReturn(new URI("/api/users"));
        } catch (URISyntaxException e) {
            throw new RuntimeException(e);
        }

        // Create a mock ServerHttpRequest with an open API path
        ServerHttpRequest openApiRequest = mock(ServerHttpRequest.class);
        try {
            when(openApiRequest.getURI()).thenReturn(new URI("/auth/login"));
        } catch (URISyntaxException e) {
            throw new RuntimeException(e);
        }

        // Create an instance of RouterValidator
        RouterValidator routerValidator = new RouterValidator();

        // Test a secured request
        Assertions.assertTrue(routerValidator.getIsSecured().test(securedRequest),
                "Secured request should return true");

        // Test an open API request
        Assertions.assertFalse(routerValidator.getIsSecured().test(openApiRequest),
                "Open API request should return false");
    }
}
