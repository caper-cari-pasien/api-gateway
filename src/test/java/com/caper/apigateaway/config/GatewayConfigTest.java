package com.caper.apigateaway.config;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.http.MediaType;
import org.springframework.test.web.reactive.server.WebTestClient;

@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
class GatewayConfigTest {

    @Autowired
    private WebTestClient webTestClient;

    @Test
    void testAuthRoute() {
        webTestClient.post().uri("http://34.126.121.92/api/auth/login")
                .contentType(MediaType.APPLICATION_JSON)
                .bodyValue("{\n" +
                        "  \"username\": \"user123\",\n" +
                        "  \"password\": \"123\"\n" +

                        "}")
                .exchange()
                .expectStatus().isOk()
                .expectBody().json("{\"message\": \"Successfully logged in\"}");
    }

}
