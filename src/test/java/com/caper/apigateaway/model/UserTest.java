package com.caper.apigateaway.model;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class UserTest {

    @Test
    void testUserModel() {
        // Create a user instance
        User user = new User();
        user.setUsername("john.doe");
        user.setPassword("password123");
        user.setRole("DOCTOR");

        // Verify the values
        Assertions.assertEquals("john.doe", user.getUsername(), "Username should match");
        Assertions.assertEquals("password123", user.getPassword(), "Password should match");
        Assertions.assertEquals("DOCTOR", user.getRole(), "Role should match");
    }
}
