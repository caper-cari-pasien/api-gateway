package com.caper.apigateaway.model;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Set;

class UserRoleTest {

    @Test
    void testUserRoleEnum() {
        // Test DOCTOR role
        Set<UserPermission> doctorPermissions = UserRole.DOCTOR.getPermissions();
        Assertions.assertEquals(2, doctorPermissions.size(), "DOCTOR role should have 2 permissions");
        Assertions.assertTrue(doctorPermissions.contains(UserPermission.DOCTOR_READ), "DOCTOR role should have DOCTOR_READ permission");
        Assertions.assertTrue(doctorPermissions.contains(UserPermission.DOCTOR_UPDATE), "DOCTOR role should have DOCTOR_UPDATE permission");

        // Test PATIENT role
        Set<UserPermission> patientPermissions = UserRole.PATIENT.getPermissions();
        Assertions.assertEquals(2, patientPermissions.size(), "PATIENT role should have 2 permissions");
        Assertions.assertTrue(patientPermissions.contains(UserPermission.PATIENT_READ), "PATIENT role should have PATIENT_READ permission");
        Assertions.assertTrue(patientPermissions.contains(UserPermission.PATIENT_UPDATE), "PATIENT role should have PATIENT_UPDATE permission");
    }
}
