package com.caper.apigateaway.model;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class UserPermissionTest {

    @Test
     void testUserPermissionEnum() {
        // Test DOCTOR_READ permission
        Assertions.assertEquals("dokter:read", UserPermission.DOCTOR_READ.getPermission(),
                "DOCTOR_READ permission should match");

        // Test DOCTOR_UPDATE permission
        Assertions.assertEquals("dokter:update", UserPermission.DOCTOR_UPDATE.getPermission(),
                "DOCTOR_UPDATE permission should match");

        // Test PATIENT_READ permission
        Assertions.assertEquals("pasien:read", UserPermission.PATIENT_READ.getPermission(),
                "PATIENT_READ permission should match");

        // Test PATIENT_UPDATE permission
        Assertions.assertEquals("pasien:update", UserPermission.PATIENT_UPDATE.getPermission(),
                "PATIENT_UPDATE permission should match");
    }
}
