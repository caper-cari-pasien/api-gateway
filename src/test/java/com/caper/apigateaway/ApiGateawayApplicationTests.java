package com.caper.apigateaway;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;

@SpringBootTest
class AuthServiceApplicationTests {

	@Test
	void testApplicationStartsWithoutExceptions() {
		assertDoesNotThrow(() -> ApiGateawayApplication.main(new String[]{}));
	}
}
