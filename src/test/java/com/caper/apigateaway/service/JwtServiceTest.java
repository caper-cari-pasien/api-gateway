package com.caper.apigateaway.service;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

class JwtServiceTest {

   private JwtService jwtService;

   private static final String SECRET_KEY = "645367566B59703373367639792F423F4528482B4D6251655468576D5A713474";

   @BeforeEach
   public void setUp() {
      jwtService = new JwtService();
   }

   @Test
   void testIsTokenNotExpired() {
      String token = generateValidToken();
      boolean isExpired = jwtService.isTokenExpired(token);
      assertFalse(isExpired, "Token should not be expired");
   }


   @Test
   void testExtractClaim() {
      String claimKey = "testClaimKey";
      String claimValue = "testClaimValue";
      Map<String, Object> claimsMap = new HashMap<>();
      claimsMap.put(claimKey, claimValue);
      String token = Jwts.builder().setClaims(claimsMap).signWith(SignatureAlgorithm.HS256, SECRET_KEY).compact();

      String extractedClaimValue = jwtService.extractClaim(token, c -> c.get(claimKey, String.class));

      assertEquals(claimValue, extractedClaimValue);
   }

   @Test
   void testExtractAllClaims() {
      Map<String, Object> claimsMap = new HashMap<>();
      claimsMap.put("testClaimKey1", "testClaimValue1");
      claimsMap.put("testClaimKey2", "testClaimValue2");
      String token = Jwts.builder().setClaims(claimsMap).signWith(SignatureAlgorithm.HS256, SECRET_KEY).compact();

      Claims extractedClaims = jwtService.extractAllClaims(token);

      assertEquals(claimsMap, extractedClaims);
   }
   String generateValidToken() {
      long expirationTimeInMillis = System.currentTimeMillis() + 3600000; // Set expiration time to 1 hour from now
      Date expirationDate = new Date(expirationTimeInMillis);

      String validToken = Jwts.builder()
              .setSubject("john.doe@example.com")
              .setExpiration(expirationDate)
              .signWith(SignatureAlgorithm.HS256, SECRET_KEY) // Use the same SECRET_KEY from JwtService class
              .compact();

      return validToken;
   }


}
